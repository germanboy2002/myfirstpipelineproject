import pytest
import urllib.request


def test_getFromWebsite():
    assert "Right-click in the box below to see one called 'the-internet'" in getFromWebsite()


def test_failInputFromWebsite():
    assert "Alibaba" in getFromWebsite()


def getFromWebsite():
    content = urllib.request.urlopen('https://the-internet.herokuapp.com/context_menu').read().decode("utf-8")
    return content
